var app = {
  initialize: function() {
    this.bindEvents();
  },
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  onDeviceReady: function() {
    initPushNotification();
  }
};

function initPushNotification() {
  var push = PushNotification.init({
    'android': {
      senderID: '1234567'
    },
    'ios': {
      alert: 'true',
      badge: 'true',
      sound: 'true'
    },
    windows: {}
  });

  /**
   *  Push通知を許可した時のコールバック
   */
  push.on('registration', function(data) {
    console.log('registration event', JSON.stringify(data));
    document.getElementById('regId').innerText = data.registrationId;
  });

  /**
   *  通知をタップしてアプリを起動した時のコールバック
   */
  push.on('notification', function(data) {
    console.log('notification event ' + JSON.stringify(data));

    push.setApplicationIconBadgeNumber(
      function() {
        console.log('success: set badge number');
      }, function(err) {
        console.log('error: can not set badge number');
      },
      data.count
    );

    push.finish(function() {
      console.log('finish sucess!');
    });
  });

  // エラー時コールバック
  push.on('error', function(err) {
    console.log('push error', err);
  });
  
  return push;
};

app.initialize();
